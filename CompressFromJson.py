#!/usr/bin/env python3

'''
@file CompressDAOD.py
@author Alaettin Serhan Mete
@brief A basic CA job that applies lossy float compression to an existing DAOD file
'''


def getCompressionLists(config_dict, key_prefix = None):
    highCompression = []
    lowCompression = []
    for key in config_dict.keys():
        item = config_dict[key]
        if isinstance(item, int):
            item_name = key_prefix + key if key_prefix is not None else key + '*'
            if item > 0:
                print(f"JSON PARSER: Adding {item_name} to High compression list.")
                highCompression.append(item_name)
            elif item < 0:
                print(f"JSON PARSER: Adding {item_name} to Low compression list.")
                lowCompression.append(item_name)
        elif isinstance(item, dict):
            hi_comp, low_comp = getCompressionLists(item, key)
            highCompression.extend(hi_comp)
            lowCompression.extend(low_comp)
        else:
            raise Exception("Unknown value type. Only int or dict are supported.")
    return highCompression, lowCompression


if '__main__' in __name__:

    high_mantissa_bits = 7
    low_mantissa_bits = 15

    # Get the user input
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--inputFiles', default=None, required=True,
                        help='Input file(s), comma-separated list')
    parser.add_argument('--outputFile', default='compressed.root',
                        help='Output file')
    parser.add_argument('--compressionConfig', default=None, required=True,
                        help='JSON file containing all branches and variables. For each, the high/low compression level can be specified. 0 equals to no compression.')
    parser.add_argument('--highMantissaBits', default=high_mantissa_bits, required=False, type=int,
                        help='Number of bits to use for the mantissa when applying high-level compression.')
    parser.add_argument('--lowMantissaBits', default=low_mantissa_bits, required=False, type=int,
                        help='Number of bits to use for the mantissa when applying low-level compression.')
    parser.add_argument('--maxEvents', default=-1, help='Maximum number of events to process. Default is all (-1)')
    args, _ = parser.parse_known_args()
    parser.add_argument('--skipEvents', default=0, help='number of events to skip. Default is 0')
    args, _ = parser.parse_known_args()

    # NEW COMPRESSION BASED ON JSON CONFIG
    import json
    with open(args.compressionConfig) as json_file:
        compression_config_dict = json.load(json_file)
    high_compression_from_json, low_compression_from_json = getCompressionLists(compression_config_dict)
    ######################################

    # Set the flags
    from AthenaConfiguration.AllConfigFlags import initConfigFlags
    flags = initConfigFlags()
    flags.Input.Files = args.inputFiles.split(',')

    flags.Exec.MaxEvents = int(args.maxEvents)
    flags.Exec.SkipEvents = int(args.skipEvents)
    
    streamToMerge = flags.Input.ProcessingTags[0].removeprefix('Stream')
    print(f'streamToMerge {streamToMerge}')
    flags.addFlag(f'Output.{streamToMerge}FileName', args.outputFile)
    flags.addFlag(f'Output.doWrite{streamToMerge}', True)
    flags.Output.doWriteDAOD = True
    flags.lock()

    # Setup the main services
    from AthenaConfiguration.MainServicesConfig import MainServicesCfg
    cfg = MainServicesCfg(flags)

    # Setup the input reading
    from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
    cfg.merge(PoolReadCfg(flags))

    # Write some metadata into TagInfo
    from EventInfoMgt.TagInfoMgrConfig import TagInfoMgrCfg
    cfg.merge(
        TagInfoMgrCfg(
            flags,
            tagValuePairs={
                "beam_type"      : flags.Beam.Type.value,    
                "beam_energy"    : str(int(flags.Beam.Energy)),   
                "IOVDbGlobalTag" : flags.IOVDb.GlobalTag,    
                "project_name"   : flags.Input.ProjectName,    
                "GeoAtlas"       : flags.GeoModel.AtlasVersion, 
                # "generatorsInfo" still needs some work to get this into TagInfoMgr correctly
                # "generatorsInfo" : flags.Input.GeneratorsInfo,   
            },  
        )   
    )

    # Write AMI tag into in-file metadata
    from PyUtils.AMITagHelperConfig import AMITagCfg
    cfg.merge(AMITagCfg(flags))
    
    from xAODMetaDataCnv.InfileMetaDataConfig import SetupMetaDataForStreamCfg
    from AthenaConfiguration.Enums import MetadataCategory
    formatString = 'DAOD_PHYSLITE'
    cfg.merge(SetupMetaDataForStreamCfg(flags, formatString))


    # Configure the output stream
    from OutputStreamAthenaPool.OutputStreamConfig import OutputStreamCfg
    from xAODMetaDataCnv.InfileMetaDataConfig import SetupMetaDataForStreamCfg
    cfg.merge(OutputStreamCfg(flags, streamToMerge))
    cfg.merge(SetupMetaDataForStreamCfg(flags, streamToMerge))

    # Setup the output stream algorithm
    # StreamDAOD = cfg.getEventAlgo(f'OutputStream{streamToMerge}')
    StreamDAOD = cfg.getEventAlgo(f'Stream{streamToMerge}')
    StreamDAOD.ForceRead = True
    StreamDAOD.TakeItemsFromInput = True

    StreamDAOD.CompressionBitsHigh = args.highMantissaBits
    StreamDAOD.CompressionBitsLow = args.lowMantissaBits
    # Uncomment for verbose output level:
    # StreamDAOD.OutputLevel = 1

    for item in flags.Input.TypedCollections:
        ctype, cname = item.split('#')
        if 'xAOD' in ctype:
            # High compression
            hi_comps = [hivar for hivar in high_compression_from_json if cname.lower() in hivar.lower()]
            if len(hi_comps) > 0:
                hi_comps = [item + '.' + hivar.split('.')[1] for hivar in hi_comps]
            # Low compression
            lo_comps = [lovar for lovar in low_compression_from_json if cname.lower() in lovar.lower()]
            if len(lo_comps) > 0:
                lo_comps = [item  + '.' + lovar.split('.')[1] for lovar in lo_comps]
            # print(f"Adding high comp: {hi_comps}")
            # print(f"Adding low  comp: {lo_comps}")
            StreamDAOD.CompressionListHigh.extend(hi_comps)
            StreamDAOD.CompressionListLow.extend(lo_comps)

    # Tune down printing
    cfg.getService(cfg.getAppProps()['EventLoop']).EventPrintoutInterval = 1000

    # cfg.getService("MessageSvc").debugLimit = 10000

    # To output full configuration in a file - allows one to figure out how to name a tool to add properties, for example.
    # with open("config.pkl", "wb") as f:
    #     cfg.store(f)

    # Now run the job
    sc = cfg.run()

    # Exit accordingly
    import sys
    sys.exit(not sc.isSuccess())
