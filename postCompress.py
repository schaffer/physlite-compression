# Add this as --postInclude "lossyCompress.lossyCompress" your DAOD job
# Needless to say, this is ONLY meant for fun local tests...

def getCompressionLists(config_dict, key_prefix = None):
    highCompression = []
    lowCompression = []
    for key in config_dict.keys():
        item = config_dict[key]
        if isinstance(item, int):
            item_name = key_prefix + key if key_prefix is not None else key + '*'
            if item > 0:
                print(f"JSON PARSER: Adding {item_name} to High compression list.")
                highCompression.append(item_name)
            elif item < 0:
                print(f"JSON PARSER: Adding {item_name} to Low compression list.")
                lowCompression.append(item_name)
        elif isinstance(item, dict):
            hi_comp, low_comp = getCompressionLists(item, key)
            highCompression.extend(hi_comp)
            lowCompression.extend(low_comp)
        else:
            raise Exception("Unknown value type. Only int or dict are supported.")
    return highCompression, lowCompression



def lossyCompress(flags, cfg):
    
    import json

    from AthenaCommon.Logging import logging
    logComp = logging.getLogger('lossyCompress')
    logComp.info('****************** STARTING lossyCompress *****************')


    jsonFileName = "InputCompressionFile.json"
    highMantissaBits =  7
    lowMantissaBits  = 20

    logComp.info('****************** lossyCompress 1 *****************')


    # NEW COMPRESSION BASED ON JSON CONFIG
    # import json
    with open(jsonFileName) as json_file:
        compression_config_dict = json.load(json_file)
    high_compression_from_json, low_compression_from_json = getCompressionLists(compression_config_dict)
    ######################################

    logComp.info('****************** lossyCompress 2 *****************')


    # Setup the output stream algorithm - assumes DAOD_PHYS production
    StreamDAOD = cfg.getEventAlgo("StreamDAOD_PHYSLITE")


    StreamDAOD.CompressionBitsHigh = highMantissaBits
    StreamDAOD.CompressionBitsLow  = lowMantissaBits
    # Uncomment for verbose output level:
    # StreamDAOD.OutputLevel = 1

    logComp.info('****************** lossyCompress 3 *****************')


    for item in flags.Input.TypedCollections:
        ctype, cname = item.split('#')
        if 'xAOD' in ctype:
            # High compression
            hi_comps = [hivar for hivar in high_compression_from_json if cname.lower() in hivar.lower()]
            if len(hi_comps) > 0:
                hi_comps = [item + '.' + hivar.split('.')[1] for hivar in hi_comps]
            # Low compression
            lo_comps = [lovar for lovar in low_compression_from_json if cname.lower() in lovar.lower()]
            if len(lo_comps) > 0:
                lo_comps = [item  + '.' + lovar.split('.')[1] for lovar in lo_comps]
            print(f"Adding high comp: {hi_comps}")
            print(f"Adding low  comp: {lo_comps}")
            StreamDAOD.CompressionListHigh.extend(hi_comps)
            StreamDAOD.CompressionListLow.extend(lo_comps)

    # # Now build the compression lists randomly, 50-50 low/high
    # for item in StreamDAOD.ItemList:
    #     ctype, cname = item.split("#")
    #     if "xAOD" in ctype and "Aux." in cname:
    #         value = random.uniform(0,1)
    #         item = item.split("Aux.")[0]+"Aux."
    #         if value >= 0.5:
    #             StreamDAOD.CompressionListHigh +=[ item+"*" ]
    #             print(f"SERHAN Compressing {item} high")
    #         else:
    #             StreamDAOD.CompressionListLow +=[ item+"*" ]
    #             print(f"SERHAN Compressing {item} low")
