import ROOT
import json

# Get the user input
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--inputFile', default=None, required=True,
                    help='Input file')
parser.add_argument('--outputJsonFile', default=None, required=False,
                    help='Output JSON file')
parser.add_argument('--defaultLevel', default=0, type=int, required=False,
                    help='Default compression level.')
args, _ = parser.parse_known_args()

in_filename = args.inputFile

if args.outputJsonFile is None:
    out_filename = in_filename + "_branches.json"
else:
    out_filename = args.outputJsonFile

in_file = ROOT.TFile.Open(in_filename)

tree = in_file.Get("CollectionTree")
lb = tree.GetListOfBranches()

branches = {}

default_compression_level = args.defaultLevel

for b in lb:
    bname = b.GetName()
    if "Aux" in bname:
        bparts = bname.split('.')
        mainpart = bparts[0] + '.'
        subpart = bparts[1]
        if branches.get(mainpart) is None:
            branches[mainpart] = {}
        if subpart:
            branches[mainpart][subpart] = default_compression_level

for b in branches.keys():
    if len(branches[b]) == 0:
        branches[b] = default_compression_level

with open(out_filename, 'w') as fp:
    json.dump(branches, fp, indent=4)

print("Done.")


